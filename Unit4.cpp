#include <iostream>
#include <array>
#include <vector>

class Vehicle
{
public:
	virtual std::ostream& print(std::ostream& out) const
	{
		out << "Vehicle";
		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Vehicle& p)
	{
		return p.print(out);
	}
};

class WaterVehicle : public Vehicle
{
public:
	WaterVehicle(const int& NewDraft = 1)
	{
		Draft = NewDraft;
	}

private:
	float Draft = 5.f;
};

class RoadVehicle : public Vehicle
{
public:
	std::ostream& print(std::ostream& out) const override
	{
		out << "Ride Height: " << RideHeight;
		return out;
	}
	void SetRideHeight(const int& NewRideHeight)
	{
		RideHeight = NewRideHeight;
	}
	float GetRideHeight()
	{
		return RideHeight;
	}

private:
	float RideHeight = 5.f;
};

class Wheel
{
public:
	Wheel()
	{
		Diameter = 1.f;
	}
	Wheel(int NewDiameter)
	{
		Diameter = float(NewDiameter);
	}
	float Diameter;
};

class Engine
{
public:
	Engine()
	{
		Power = 1.f;
	}
	Engine(int Power)
	{
		this->Power = float(Power);
	}
public:
	float Power = 1.f;
};

class Bicycle : public RoadVehicle
{
public:
	Bicycle()
	{
		Wheels = { Wheel(), Wheel() };
	}
	Bicycle(const Wheel& Wheel1, const Wheel& Wheel2, const int& NewRideHeight)
	{
		Wheels = { Wheel1, Wheel2 };
		SetRideHeight(NewRideHeight);
	}
	std::ostream& print(std::ostream& out) const override
	{
		out << "Bicycle ";
		out << "Wheels: ";
		for (Wheel Wheel : Wheels)
		{
			out << Wheel.Diameter << ' ';
		}
		return RoadVehicle::print(out);
	}

private:
	std::array<Wheel, 2> Wheels;
};

class Car : public RoadVehicle
{
public:
	Car()
	{
		Wheels = { Wheel(), Wheel(), Wheel(), Wheel() };
		SetEngine = Engine();
	}
	Car(const Engine& NewEngine, const Wheel& Wheel1, const Wheel& Wheel2, const Wheel& Wheel3, const Wheel& Wheel4, const int& NewRideHeight)
	{
		SetEngine = NewEngine;
		Wheels = { Wheel1, Wheel2, Wheel3, Wheel4 };
		SetRideHeight(NewRideHeight);
	}
	std::ostream& print(std::ostream& out) const override
	{
		out << "Car ";
		out << "Engine: " << SetEngine.Power << ' ';
		out << "Wheels: ";
		for (Wheel Wheel : Wheels)
		{
			out << Wheel.Diameter << ' ';
		}
		return RoadVehicle::print(out);
	}
	float GetEnginePower()
	{
		return SetEngine.Power;
	}

private:
	std::array<Wheel, 4> Wheels;
	Engine SetEngine;
};

class Point
{
public:
	Point(const int& x = 1, const int& y = 1, const int& z = 1)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

private:
	int x;
	int y;
	int z;
};

class Circle : public Vehicle
{
public:
	Circle(const Point& NewCenter, const int& NewRadius)
	{
		Center = NewCenter;
		Radius = NewRadius;
	}

private:
	Point Center;
	int Radius;
};

float getHighestPower(const std::vector<Vehicle*>& v)
{
	float MaxPower = 0;
	for (Vehicle* Element : v)
	{
		Car* Ptr = static_cast<Car*>(Element);
		if (Ptr)
		{
			MaxPower = (MaxPower < Ptr->GetEnginePower()) ? Ptr->GetEnginePower() : MaxPower;
		}
	}
	return MaxPower;
}

void printVector(const std::vector<Vehicle*>& v)
{
	for (auto& i : v)
	{
		std::cout << *i << '\n';
	}
}

void deleteVector(const std::vector<Vehicle*>& v)
{
	for (auto i = begin(v); i != end(v); i++)
	{
		delete* i;
	}
}

int main()
{
	Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
	std::cout << c << '\n';
	Bicycle t(Wheel(20), Wheel(20), 300);
	std::cout << t << '\n';


	std::vector<Vehicle*> v;

	v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));

	v.push_back(new Circle(Point(1, 2, 3), 7));

	v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));

	v.push_back(new WaterVehicle(5000));

	printVector(v);

	std::cout << "The highest power is" << getHighestPower(v) << '\n';	  // ���������� ��� �������

	deleteVector(v);
	return 0;
}